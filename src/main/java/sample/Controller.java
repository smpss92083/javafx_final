package sample;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ImageView slogan;
    public ComboBox cbLocation;
    public Button btnChoose;
    public Label lbLocation;
    public Label lbTempHigh;
    public Label lbTempLow;
    public Label lbWeather;
    public Label lbRain;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File img = new File("src/main/java/resources/title.png");
        slogan.setImage(new Image(img.toURI().toString()));

        cbLocation.getItems().addAll(
                "台中市","台北市","新北市","桃園市","台南市","高雄市","基隆市","新竹市","嘉義市","新竹縣","苗栗縣","彰化縣","南投縣","雲林縣","嘉義縣","屏東縣","宜蘭縣","花蓮縣","台東縣","澎湖縣","金門縣","連江縣"
        );
    }

    public void selectLoacation(ActionEvent actionEvent) {

        String loc = cbLocation.getSelectionModel().getSelectedItem().toString();
        lbLocation.setText(loc);
        cbLocation.getItems();

    }
}
